// ==UserScript==
// @name         DA DoA Assist
// @namespace    https://gitlab.com/da-doa/assist
// @version      1.1.1
// @description  Send assist requests to Discord.
// @author       cz4r [2316631] & TornOne [2225439] & Heasleys4hemp [1468764] & josephting [2272298]
// @match      https://www.torn.com/loader.php?sid=attack*
// @grant        GM_addStyle
// @grant        GM.xmlHttpRequest
// @connect      assist-bot.vercel.app
// @downloadURL    https://gitlab.com/da-doa/assist/raw/master/assist.user.js
// @updateURL    https://gitlab.com/da-doa/assist/raw/master/assist.user.js
// ==/UserScript==

let bankStyles = `
.wb_attack_content.da {
    background-color: #f1f1f1;
    border: 1px solid rgba(0, 0, 0, 0.5);
    border-radius: 5px;
    margin-top: 10px;
    margin-left: auto;
    margin-right: auto;
    align-items: center;
    padding-top: 4px;
    padding-bottom: 4px;
    text-align: center;
    box-shadow: 0 0 0 3px rgb(33, 100, 255);
}
.wb_attack_content.da .wb-attack-request-button {
    background: transparent linear-gradient(180deg,#CCCCCC 0%,#999999 60%,#666666 100%) 0 0 no-repeat;
    cursor: pointer;
    font-family: Arial,sans-serif;
    font-size: 14px;
    font-weight: 700;
    text-align: center;
    letter-spacing: 0;
    color: #333;
    text-shadow: 0 1px 0 #ffffff66;
    text-transform: uppercase;
    padding: 4px 8px;
    border-radius: 10px;
}
.wb_attack_content.da .wb-attack-request-button:hover {
    color: #757575;
}
.wb_attack_content.da .wb-attack-request-button.wb-disabled {
    background: transparent linear-gradient(180deg,#999999 0%,#CCCCCC 100%) 0 0 no-repeat;
    cursor: not-allowed;
    color: #777;
    box-shadow: 0 1px 0 #ffffffa6;
    text-shadow: 0 -1px 0 #ffffff66;
}
`;

GM_addStyle(bankStyles);

let chatScript = document.querySelector('script[uid][name]');
let attacker = {
    name: chatScript.getAttribute('name'),
    uid: chatScript.getAttribute('uid')
};
let opponent = {
    name: '',
    uid: ''
};
const url = 'https://assist-bot.vercel.app/api/assist';

// Assist Request Element
let assistElement = document.createElement('div');
assistElement.setAttribute('class', 'wb_attack_content da');
assistElement.innerHTML = '<button id="daAssistRequest" class="wb-attack-request-button">Request Help from ÐA</button>';

const observer = new MutationObserver(function() {
    // Add Request Button
    let urlCheck = window.location.href;
    if (urlCheck.indexOf("loader.php?sid=attack&") > 0) {
        if (document.contains(document.querySelector('#mainContainer div[class^=playersModelWrap] div[class^=logStatsWrap]'))) {
            document.querySelector('#mainContainer div[class^=playersModelWrap]').insertBefore(assistElement, document.querySelector('#mainContainer div[class^=playersModelWrap] div[class^=logStatsWrap]'));
            initButton();
            observer.disconnect();
        }
    }
});
observer.observe(document, { childList: true, subtree: true });

// Request Assist
const initButton = function() {
    document.querySelector('#daAssistRequest').addEventListener('click', function(e) {
        e.preventDefault();

        document.querySelectorAll('.user-name').forEach(el => {
            if (name !== el.innerHTML) {
                opponent.name = el.innerHTML;
            }
        });

        var b = e.target;
        b.classList.toggle('wb-button');
        b.classList.toggle('wb-disabled');
        b.disabled = true;
        var val = b.innerHTML;
        b.innerHTML = 'Requesting Help from ÐA...';

        let requestData = {
            attacker: attacker,
            opponent: opponent,
            url: window.location.href
        };
        GM.xmlHttpRequest({
            method: "POST",
            url: url,
            data: JSON.stringify(requestData),
            headers: {
                "Content-Type": "application/json"
            }
        });
        window.setTimeout(function(){ b.classList.toggle('wb-button');b.classList.toggle('wb-disabled');b.disabled = false; b.innerHTML = val; }, 5000);
    });
};
